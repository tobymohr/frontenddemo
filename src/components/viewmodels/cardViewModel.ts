export default class CardViewModel {
    title: string = '';
    text: string = '';
    imageUrl: string = '';
}
