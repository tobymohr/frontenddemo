import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { RootState } from '@/store/statetypes';
import { students } from '@/store/modules/students/module';

Vue.use(Vuex);
const storeOptions: StoreOptions<RootState> = {
  state: {
    version: '1.0.0'
  },
  modules: {
    students
  }
};
const store = new Vuex.Store<RootState>(storeOptions);
export default store;
