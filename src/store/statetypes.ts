import StudentModel from '@/models/studentModel';

export interface RootState {
    version: string;
}
export interface StudentState {
    students: StudentModel[];
}