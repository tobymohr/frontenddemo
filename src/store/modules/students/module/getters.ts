import { GetterTree } from 'vuex';
import { RootState, StudentState } from '@/store/statetypes';
import StudentModel from '@/models/studentModel';

export const getters: GetterTree<StudentState, RootState> = {
  getStudents(state): StudentModel[] {
    return state.students;
  }
};
