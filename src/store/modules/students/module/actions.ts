import { ActionTree } from 'vuex';
import { StudentState } from '@/store/statetypes';
import { RootState } from '@/store/statetypes';
import * as networking from '@/networking/httpUtils';
import StudentModel from '@/models/studentModel';

export const actions: ActionTree<StudentState, RootState> = {
  async fetchStudents({ commit }, context): Promise<object> {
    try {
      const { status: status, data: data } = await networking.get('students');
      if (status === 200) {
        commit('studentsLoaded', data as StudentModel[]);
      }
      return { status, data };
    } catch (error) {
      return { status: 500, body: error };
    }
  }
};
