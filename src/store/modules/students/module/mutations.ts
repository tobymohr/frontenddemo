import { MutationTree } from 'vuex';
import { StudentState } from '@/store/statetypes';
import StudentModel from '@/models/studentModel';

export const mutations: MutationTree<StudentState> = {
  studentsLoaded(state, payload: StudentModel[]) {
    state.students = payload;
  }
};
