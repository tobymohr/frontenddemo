import { Module } from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { StudentState } from '@/store/statetypes';
import { RootState } from '@/store/statetypes';

const namespaced: boolean = true;

export const state: StudentState = {
  students: []
};

export const students: Module<StudentState, RootState> = {
  namespaced,
  state,
  getters,
  actions,
  mutations
};
