export default class StudentModel {
    _id: string = '';
    name: string = '';
    text: string = '';
    profilePicturePath: string = '';
}
