export default class UrlBuilder {
  private url: string;
  private parameters: { [key: string]: string } = {};

  constructor(url: string) {
    this.url = url;
  }

  addParameter(key: string, value: string) {
    if (value) {
      this.parameters[key] = value;
    }
  }

  setUrl(url: string) {
    this.url = url;
  }

  getUrl(): string {
    return this.url;
  }

  generateUrlWithParams(): string {
    const keys = [...Object.keys(this.parameters)];
    for (let i = 0; i < keys.length; i++) {
      const param = i === 0 ? '?' : '&';
      this.url += param + keys[i] + '=' + this.parameters[keys[i]];
    }
    return this.url;
  }
}
