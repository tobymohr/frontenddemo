import axios, { AxiosPromise } from 'axios';

export const BASE_IP = 'http://localhost:3001';
const BASE_API_URL = `${BASE_IP}/api/v1/`;
const standardClient = axios.create({ baseURL: BASE_API_URL });

export function get(url: string): AxiosPromise {
  return standardClient.get(BASE_API_URL + url);
}

export function post(url: string, payload: any): AxiosPromise {
  return standardClient.post(BASE_API_URL + url, payload);
}

export function put(url: string, payload: any): AxiosPromise {
  return standardClient.put(BASE_API_URL + url, payload);
}

export function del(url: string): AxiosPromise {
  return standardClient.delete(BASE_API_URL + url);
}
